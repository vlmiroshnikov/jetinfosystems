package services

import java.net.URL

import org.scalatest.{Matchers, FlatSpec}

class SearchServiceTest extends FlatSpec with Matchers {
  ignore should "" in {
    val r  = Seq("aa", "bb", "aa").foldLeft(Map.empty[String, Int])(
      (agg, item) => {
        val count = agg.getOrElse(item, 0)
        agg.updated(item, count + 1)
      })
    r("aa") shouldBe 2
    r("bb") shouldBe 1
  }

  it should "a" in {
    val url = "http://me.baklushabit.ya.ru/replies.xml?item_no=8382&amp;parent_id=8395"
    val h = new URL(url).getHost
    val pattern = """[^.]*\.[^.]{2,3}$""".r
    println(pattern findFirstIn h)
  }
}
