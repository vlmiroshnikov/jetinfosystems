package services

import java.net.URL
import java.util.concurrent.{Callable, Executors, TimeUnit}

import dto.{Query, SearchResult}
import play.api.libs.ws.WS

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration.Duration
import scala.util.Try

trait SearchService {
  def search(q: Query) : Future[SearchResult]
}

class SearchServiceImpl extends SearchService {
  val url = "http://blogs.yandex.ru/search.rss"
  val maxConnections = 10


  override def search(q: Query): Future[SearchResult] = {
    future {
      val pool = Executors.newFixedThreadPool(maxConnections)
      val collected = q.keywords.map { k =>
        pool.submit(getCallable(k))
      }
      pool.shutdown()
      pool.awaitTermination(60, TimeUnit.SECONDS)

      val rez = collected.map { c => Try(c.get).getOrElse(Seq.empty)}.flatten
        .foldLeft(Map.empty[String, Int])(
          (agg, item) => {
            val count = agg.getOrElse(item, 0)
            agg.updated(item, count + 1)
          })
      SearchResult(rez)
    }
  }

  private def getCallable(key: String): Callable[Seq[String]] = new Callable[Seq[String]] { def call = build(key) }

  private def build(key:String) = {
    Await.result(WS.url(url)
      .withQueryString(("text", key)).get()
      .map {
      r =>
        (r.xml \\ "channel" \\ "item" \\ "link")
          .map { x => getHost(x.text)}
    }, Duration(10, TimeUnit.SECONDS))
  }


  val pattern = """[^.]*\.[^.]{2,3}$""".r

  private def getHost(s:String)  = {
    val host = new URL(s).getHost
    pattern.findFirstIn(host).getOrElse(host)
  }
}





