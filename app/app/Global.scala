import controllers.SearchController
import play.api.GlobalSettings
import services.SearchServiceImpl

object Global extends GlobalSettings {
  override def getControllerInstance[A](controllerClass: Class[A]): A = {
    new SearchController(new SearchServiceImpl).asInstanceOf[A]
  }
}
