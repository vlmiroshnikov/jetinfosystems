package dto

import play.api.mvc._

case class Query(keywords: Seq[String])

object Query {
  implicit def queryStringBinder(implicit binder: QueryStringBindable[String]) = new QueryStringBindable[Query] {
    override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, Query]] = {
      for {
        keys: Either[String, String] <- binder.bind(key, params)
      } yield {
        keys match {
          case Right(keywords) =>
            val q = params(key)
            Right(Query(q))
          case Left(message) => Left(message)
        }
      }
    }
    override def unbind(key: String, query: Query): String = {
      query.keywords.reduceLeft((agg, item) => agg + "&" + binder.unbind(key, item))
    }
  }
}
