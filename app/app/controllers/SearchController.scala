package controllers

import dto.{Query, SearchResult}
import play.api.http.ContentTypes
import play.api.libs.json.{JsValue, Json, Writes}
import play.api.mvc._
import services.SearchService

import scala.concurrent.ExecutionContext.Implicits.global

class SearchController(val searchService: SearchService) extends Controller {

  import controllers.SearchController._
  implicit val formatter  = resultFormatter

  def search(query: Query) = Action.async {
    searchService.search(query).map {
      s => Ok(Json.prettyPrint(Json.toJson(s))).as(ContentTypes.JSON)
    }
  }
}

object SearchController {
  val resultFormatter = new Writes[SearchResult] {
    override def writes(o: SearchResult): JsValue = {
      Json.toJson(o.data)
    }
  }
}
